#include <wiring_private.h>


//CONFIG PINS
int pinInTracao = 9;//PWM In
int pinOut1Tracao = 11;//PWM Out
int pinOut2Tracao = 10;//PWM Out
//int pinInLuz = 5;//PWM In
//int pinOutLuz = 6;//PWM Out


//CONFIGS Faixas
int analogicoMediano = -1;
int analogicoMax = 0;
int analogicoMin = 0;
int analogicoWriteMax = 255;

//Variaveis luz
//int analogicoLuzMin = -1;
//int analogicoLuzMax = -1;


void setup() {
  //Setup de tra��o
  pinMode(pinInTracao, INPUT);
  pinMode(pinOut1Tracao, OUTPUT);
  pinMode(pinOut2Tracao, OUTPUT);
  
  //Setup lampada
//  pinMode(pinInLuz, INPUT);
//  pinMode(pinOutLuz, OUTPUT);
//  int analogico = pulseIn (pinInLuz, HIGH);
//  if(analogicoLuzMin == -1){
//    analogicoLuzMin = analogico;
//  }
//  if(analogicoLuzMax == -1){
//    analogicoLuzMax = analogico;
//  }


  //Inicializa o mediano
  analogicoMediano = pulseIn (pinInTracao, HIGH);
  analogicoMax = analogicoMediano;
  analogicoMin = analogicoMediano;


  //Inicializa a comunica��o serial (para testes)
  //Serial.begin(9600);
}




/**
 * (1109) - (1578) - (1938)
 */
void executaAcaoTracao(int analogico){
  //Serial.println(analogico);
  if(analogico == 0){
    analogicoMin = analogicoMediano;
    analogicoMax = analogicoMediano;

    digitalWrite(pinOut1Tracao, LOW);
    digitalWrite(pinOut2Tracao, LOW);
    return;
  }

  if(analogico > analogicoMax){
    analogicoMax = analogico;
  }else if(analogico < analogicoMin){
    analogicoMin = analogico;
  }


  int analogicoWrite = -1;
  float percentFaixa = 0;


  if(analogicoMediano-50 < analogico && analogico < analogicoMediano+50 ){
    digitalWrite(pinOut1Tracao, LOW);
    digitalWrite(pinOut2Tracao, LOW);
    //analogicoMediano = analogico;

    return;


  }else if(analogico > analogicoMediano) {
    float diffMaxMed = analogicoMax - analogicoMediano;
    float diffMaxAnalogico = analogicoMax - analogico;
    float diff = diffMaxMed - diffMaxAnalogico;
    percentFaixa = 100*diff/diffMaxMed;
    analogicoWrite = percentFaixa*analogicoWriteMax/100;
    if(analogicoWrite > 255){
      analogicoWrite = 255;
    }
    analogWrite(pinOut2Tracao, analogicoWrite);
    digitalWrite(pinOut1Tracao, LOW);

  }else{
    float diffMedMin = analogicoMediano - analogicoMin;
    float diffMedAnalogico = analogicoMediano - analogico;
    float diff = diffMedMin - diffMedAnalogico;

    percentFaixa = (100*diff/diffMedMin)-100;
    if(percentFaixa != 0){
      percentFaixa = percentFaixa*-1;
    }

    analogicoWrite = percentFaixa*analogicoWriteMax/100;
    if(analogicoWrite > 255){
      analogicoWrite = 255;
    }
    analogWrite(pinOut1Tracao, analogicoWrite);
    digitalWrite(pinOut2Tracao, LOW);
  }

  /*Serial.print("analogico:");Serial.print(analogico);
  Serial.print(" :analogicoWrite:");Serial.print(analogicoWrite);
  Serial.print(" :percentFaixa:");Serial.print(percentFaixa);
  Serial.print(" :analogicoMin:");Serial.print(analogicoMin);
  Serial.print(" :analogicoMediano:");Serial.print(analogicoMediano);
  Serial.print(" :analogicoMax:");Serial.println(analogicoMax);*/
}


void loop() {
  while(true){
    executaAcaoTracao(pulseIn (pinInTracao, HIGH));
    //executaAcaoLampada(pulseIn (pinInLuz, HIGH));
    //delay(100);
  }

}

//1002 a 2019 
//void executaAcaoLampada(int analogico){
//  int analogicoWriteMin = 0;
//  int analogicoWriteMax = 255;
//
//  if(analogico < analogicoLuzMin){
//    analogicoLuzMin = analogico;
//  }
//
//  if(analogico > analogicoLuzMax){
//    analogicoLuzMax = analogico;
//  }
//
//  int diffMaxMin = analogicoLuzMax - analogicoLuzMin;
//
//  float faixaAnalogico = analogico - analogicoLuzMin;
//  float percent = faixaAnalogico * 100/diffMaxMin;
//
//  int valorPwmLuz = percent * analogicoWriteMax/100;
//  
//  if(valorPwmLuz > 255){
//    valorPwmLuz = 255;
//  }
//  
//  analogWrite(pinOutLuz, valorPwmLuz);
//
//  //INFOS
//  /*Serial.print(" analogicoLuzMin:");Serial.print(analogicoLuzMin);
//  Serial.print(" analogicoLuzMax:");Serial.print(analogicoLuzMax);
//  Serial.print(" diffMaxMin:");Serial.print(diffMaxMin);
//  Serial.print(" faixaAnalogico:");Serial.print(faixaAnalogico);
//  Serial.print(" analogico:");Serial.print(analogico);
//  Serial.print(" percent:");Serial.print(percent);
//  Serial.print(" valorPwmLuz:");Serial.println(valorPwmLuz);*/
//}
